package com.blueshifttestt;

import android.app.Application;
import android.support.v4.content.ContextCompat;

import com.blueshifttestt.packages.RNBlueshiftPackage;
import com.facebook.react.ReactApplication;
import info.applike.advertisingid.RNAdvertisingIdPackage;
import io.invertase.firebase.RNFirebasePackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import com.blueshift.Blueshift;
import com.blueshift.model.Configuration;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNAdvertisingIdPackage(),
            new RNFirebasePackage(),
              new RNBlueshiftPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Configuration configuration = new Configuration();
      // == Mandatory Settings ==
  configuration.setAppIcon(R.mipmap.ic_launcher);
  configuration.setApiKey("b6646b412d86cab8ee97ac82a7e1dac8");


  // == Notification (Optional) ==
  configuration.setLargeIconResId(R.drawable.common_google_signin_btn_icon_dark);
  configuration.setSmallIconResId(R.drawable.common_google_signin_btn_icon_dark_focused);
  int color = ContextCompat.getColor(getApplicationContext(), R.color.white);
  configuration.setNotificationColor(color);
  configuration.setDialogTheme(R.style.AppTheme); // for dialog type notifications

  // == Notification Channel (Android O and above) ==
  // Optional: if not set and not found in payload, SDK will assign a default id (bsft_channel_General).
  configuration.setDefaultNotificationChannelId("FSHealth");
  // Optional: if not set and not found in payload, SDK will assign a default name (General).
  configuration.setDefaultNotificationChannelName("FSHealth");
  // optional: only set if present in payload or config object
  configuration.setDefaultNotificationChannelDescription("app_channel_description_default");



  Blueshift.getInstance(this).initialize(configuration);
    SoLoader.init(this, /* native exopackage */ false);
  }
}
