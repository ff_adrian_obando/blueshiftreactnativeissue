package com.blueshifttestt.module;

import com.blueshift.Blueshift;
import com.blueshift.model.UserInfo;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;

import java.util.HashMap;
import org.jetbrains.annotations.NotNull;


@ReactModule(name = "RNBlueshift")
public class RNBlueshiftModule extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;

    public RNBlueshiftModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }
    @Override
    public String getName() {
        return "RNBlueshift";
    }

    @ReactMethod
    public void userLogin(@NotNull String email, @NotNull String memberId,@NotNull String deviceId){
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", email);
        params.put("customer_id", memberId);
        UserInfo userInfo = UserInfo.getInstance(reactContext);
        userInfo.setEmail(email);
        userInfo.setRetailerCustomerId(memberId);
        userInfo.save(reactContext);
        Blueshift.getInstance(reactContext).identifyUserByDeviceId(deviceId, params, false);


    }
}
