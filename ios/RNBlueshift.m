
//
//  RNBlueshift.m
//  FirstStopHealth
//
//  Created by FF_Adrian on 11/21/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "RNBlueshift.h"

@implementation RNBlueshift

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(userLogin:(NSString *)email customerId:(NSString *)customerId)
{
  
  NSDictionary *details = @{
                              @"email":email,
                              @"customer_id":customerId,
  };
  
  [[BlueShiftUserInfo sharedInstance] setEmail:email];
  [[BlueShiftUserInfo sharedInstance] setRetailerCustomerID:customerId];


  [[BlueShiftUserInfo sharedInstance] save];
   

  [[BlueShift sharedInstance] identifyUserWithDetails:details canBatchThisEvent:NO];

 
}

@end
