//
//  RNBlueshift.h
//  FirstStopHealth
//
//  Created by FF_Adrian on 11/21/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <React/RCTBridgeModule.h>
#import <BlueShift-iOS-SDK/BlueShift.h>

@interface RNBlueshift : NSObject <RCTBridgeModule>
@end
